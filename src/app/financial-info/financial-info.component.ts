import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-financial-info',
  templateUrl: './financial-info.component.html',
  styleUrls: ['./financial-info.component.scss']
})
export class FinancialInfoComponent implements OnInit {

  years = [
    // { id: 1, yr: 2019, show: false },
    { id: 1, yr: 2019, show: false },
    { id: 2, yr: 2018, show: false }
  ];

  constructor() { }

  ngOnInit() {
    window.scroll(0, 0);
  }

  accordianClicked(event) {
    const id = +event.target.id;

    for (const d of this.years) {
      if (id === d.id) {
        d.show = d.show = !d.show;
      } else {
        d.show = false;
      }
    }
  }
}
