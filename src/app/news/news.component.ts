import { Component, OnInit } from '@angular/core';
import { NewsServiceService } from '../shared/services/news-service.service';
import { WordpressService } from '../shared/services/wordpress.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})

export class NewsComponent implements OnInit {
  newsPosts;
  title;
  content;
  date;
  data = [];
  bgColor: string;
  externalPosts;

  constructor(private news: NewsServiceService, private wp: WordpressService, private route: Router) {
    this.data = news.ns;
  }

  ngOnInit() {
    window.scroll(0, 0);

    setTimeout(() => {
      for (let n = 0; n < this.data.length; n++) {
        switch (this.data[n].header) {
          case 'In the News':
            this.bgColor = '#30bbad'; // teal
            break;

          case 'Announcement':
            this.bgColor = '#98c93e'; // lime green
            break;

          case 'Article':
            this.bgColor = '#296a6d'; // blue
            break;

          default:
            this.bgColor = '#30bbad'; // lime green
            break;
        }
        document.getElementById('tf' + [n]).style.backgroundColor = this.bgColor;
        document.getElementById('hd' + [n]).style.color = this.bgColor;
      }
    }, 100);
    
    this.wp.getNewsPosts()
      .subscribe(
        (d: any) => {
          this.newsPosts = d;
          for (let i = 0; i < d.length; i++) {
            this.title = d[i].title.rendered;
            this.content = d[i].content.rendered;
            this.date = d[i].date;
            this.date = new Date(this.date);
            //console.log(this.newsPosts);
          }
        }
      );

    this.wp.getExternalPosts()
      .subscribe(
        (d: any) => {
          this.externalPosts = d;
          for (let i = 0; i < d.length; i++) {
            this.title = d[i].title.rendered;
            this.content = d[i].content.rendered;
            this.date = d[i].date;
            this.date = new Date(this.date);
            //console.log(this.content);
          }
        }
      );

    // directToNewsPost(id: any) {
    //   // console.log(this.route);
    //   this.route.navigate(['news/article', id]);
    // }
  }
}
