import { Component, OnInit, Input } from '@angular/core';
import { NewsServiceService } from '../../shared/services/news-service.service';
import { ActivatedRoute, Params } from '@angular/router';
import { WordpressService } from '../../shared/services/wordpress.service';

@Component({
  selector: 'app-news-article',
  templateUrl: './news-article.component.html',
  styleUrls: ['./news-article.component.scss']
})
export class NewsArticleComponent implements OnInit {
  newsPosts;
  title;
  date;
  content;

  @Input() id: number;

  localArticles = [];

  constructor(private articles: NewsServiceService, private route: ActivatedRoute, private wp: WordpressService) {
    //this.getNewsPosts();
    // this.localArticles = articles.fullArtcles;
  }

  ngOnInit() {
    window.scroll(0, 0);
    // get the id from the route and append it the the URL
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
        }
      );

    for (const a of this.articles.fullArtcles) {
      if (a.id === this.id) {
        this.localArticles = [...this.localArticles, a];
      }
    }

    //getNewsPosts() {
      this.wp.getNewsPosts()
        .subscribe(
          (d: any) => {
            this.newsPosts = d;
            for (let i = 0; i < d.length; i++) {
              this.title = d[i].title.rendered;
              this.date = d[i].date;
              this.content = d[i].content.rendered;
              
              console.log(this.newsPosts[i].categories[0]);
            }
          }
        );
    //}
  }

}
