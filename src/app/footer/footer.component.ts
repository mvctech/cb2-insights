import {Component, OnInit} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {ActivatedRoute, Route, Router} from '@angular/router';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  closeIconClicked: boolean;
  previouslyClosed: boolean;
  address: String;
  isContact: Boolean = false;
  isInvestor: Boolean = false;
  copyrightYear;

  constructor(private cookieService: CookieService, private route: Router) {
  }
  // constructor(private route: Router) {
  // }
  ngOnInit() {
    this.route.events.subscribe((val) => {
      if ((this.route.routerState.snapshot.url === '/contact') || (this.route.routerState.snapshot.url === '/investor')) {
        this.isContact = true;
      } 
      else {
        this.isContact = false;
      }
    });

    window.scroll(0, 0);
    this.address = '?q=5045 Orbitor Dr Building 11 Suite 300, Mississauga, ON L4W 4Y4';
    this.previouslyClosed = this.cookieService.check('Modal');

    if (this.previouslyClosed) {
      return this.closeIconClicked = true;
    }
    this.closeIconClicked = false;

    this.copyrightYear = new Date().getFullYear();
    // console.log(this.copyrightYear);
    document.querySelector(".footer-copyright-year").innerHTML = this.copyrightYear;
    document.querySelector(".mobile-footer-copyright-year").innerHTML = this.copyrightYear;

  }

  // closeCookie(event) {
  // }
  closeCookieModal() {
    this.cookieService.set('Modal', 'Footer Modal on CB2 Insights');
    this.closeIconClicked = true;
  }
}
