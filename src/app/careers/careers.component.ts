import {Component, OnInit } from '@angular/core';
import {WordpressService} from '../../app/shared/services/wordpress.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-careers',
  templateUrl: './careers.component.html',
  styleUrls: ['./careers.component.scss']
})

export class CareersComponent implements OnInit {
  jobPostings;
  show;

  itemList1 = [
    {
      title: 'Create', imgUrl: 'group-9-copy@2x.png',
      content: 'We build evidence-based, data-driven, tools that are best-in-breed and world-class through and through.'
    },
    {
      title: 'Inspire', imgUrl: 'group-12-copy@2x.png',
      content: 'We inspire the industry every day through our software and services that not only exceed expectations but fuel new ones.'
    },
    {
      title: 'Empower', imgUrl: 'group-17-copy@2x.png',
      content: 'We empower our team to do what they believe is right and give them the room to drive initiatives from start to finish.'
    }
  ];

  imgList = [
    { imgUrl: 'sail-office-16-copy@3x.png' },
    { imgUrl: 'sail-office-32-copy@3x.png' },
    { imgUrl: 'sail-office-29-copy@3x.png' },
    { imgUrl: 'sail-office-15-copy@3x.png' },

    // { imgUrl: 'sail-office-16@2x.jpg' },
    // { imgUrl: 'sail-office-32@2x.jpg' },
    // { imgUrl: 'sail-office-29-copy@2x.jpg' },
    // { imgUrl: 'sail-office-15@2x.jpg' },
  ];

  itemList2 = [
    {
      title: 'We are data-driven', imgUrl: 'group-10-copy@2x.png',
      content: 'Everything we do is driven by the data that we collect and the value we provide back into the industry. We champion every insight that our solutions create.'
    },
    {
      title: 'We never give up', imgUrl: 'group-7-copy@2x.png',
      content: 'Life can get hard but we never give up. We know that our teammates have our back and will support us to the finish line. We run hard and know we’ve got fans all around us.'
    },
    {
      title: 'We seize every opportunity', imgUrl: 'group-11-copy@2x.png',
      content: 'Every prospective partner, system integration, feature request is a potential opportunity. We leave no stone unturned and optimize every outcome.'
    },
    {
      title: 'We are all one', imgUrl: 'group-23-copy@2x.png',
      content: 'We are a family. Everyone plays their role and everyone is a key component. We succeed together and therefore always work towards the greater good for all of us.'
    },
  ];

  constructor(private wp: WordpressService, private route: Router) {
    this.getJobPostings();
  }

  ngOnInit() {
    window.scroll(0, 0);
    console.log(this.route);
  }

  getJobPostings() {
    this.wp.getData()
    .subscribe(
      (d: any) => {
        this.jobPostings = d;
        for (let j = 0; j <= 2; j++) {
          for(let i = 0; i < d.length; i++){
            if(d[i].categories[0] !== 19){
              this.jobPostings[i].categories = d[i].categories[0];
              this.show = true;
            }
            else {
              this.jobPostings[i].categories = d[i].categories[1];
            }          
          }
        }
      }
    ); 
  }

  directToDetails(id: any) {
    //console.log(this.route);
    this.route.navigate(['jobs/job-details', id]);
  }
}






































