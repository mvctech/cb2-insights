import { Component, OnInit, AfterViewInit } from '@angular/core';
import { StockInformationService } from '../shared/services/stock-information.service';
let thisComponent: any;

@Component({
  selector: 'app-stock-data',
  templateUrl: './stock-data.component.html',
  styleUrls: ['./stock-data.component.scss']
})
export class StockDataComponent implements OnInit {

  dd = new Date().getDate();
  mm = new Date().getMonth();
  yy = new Date().getFullYear();
  hh = new Date().getHours();
  mmm = new Date().getMinutes();

  monthNames = ['Jan', 'Feb', 'Mar', 'Ap', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
  ];

  date = this.monthNames[this.mm] + ' ' + this.dd + ', ' + this.yy;
  date2 = new Date().toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });

  arrowType: String;

  usShortName: String;
  caShortName: String;
  shortName: String;

  usCurrentPrice: Number;
  caCurrentPrice: Number;
  currentPrice: Number;

  usChange: Number;
  change: Number;
  
  usChangePercent: Number;
  changePercent: Number;

  usVolume: Number;
  volume: Number;

  usMarketCap: Number;
  marketCap: Number;

  usDayRangeHigh: Number;
  dayRangeHigh: Number;

  usDayRangeLow: Number;
  dayRangeLow: Number;

  usWeekRangeHigh: Number;
  weekRangeHigh: Number;

  usWeekRangeLow: Number;
  weekRangeLow: Number;

  usVolumeValue: String = 'vv';
  volumeValue: String = 'vv';

  usMarketValue: String = 'mv';
  marketValue: String = 'mv';

  constructor(private data: StockInformationService) {
    this.getInfo();
  }

  ngOnInit() {
    window.scroll(0, 0);
    thisComponent = this;
  }

  checkValue(totalCount, itemValue) {
    if (totalCount > 999 && totalCount < 999999) {
      switch (itemValue) {
        case 'mv':
          this.marketValue = 'k';
          break;
        case 'vv':
          this.volumeValue = 'k';
          break;
      }
      return;
    }
    if (totalCount > 999999) {
      switch (itemValue) {
        case 'mv':
          this.marketValue = 'mm';
          break;
        case 'vv':
          this.volumeValue = 'mm';
          break;
      }
      return;
    }
    if (totalCount < 999999) {
      switch (itemValue) {
        case 'mv':
          this.marketValue = '';
          break;
        case 'vv':
          this.volumeValue = '';
          break;
      }
      return;
    }
  }

  usCheckValue(totalCount, itemValue) {
    if (totalCount > 999 && totalCount < 999999) {
      switch (itemValue) {
        case 'mv':
          this.usMarketValue = 'k';
          break;
        case 'vv':
          this.usVolumeValue = 'k';
          break;
      }
      return;
    }
    if (totalCount > 999999) {
      switch (itemValue) {
        case 'mv':
          this.usMarketValue = 'mm';
          break;
        case 'vv':
          this.usVolumeValue = 'mm';
          break;
      }
      return;
    }
    if (totalCount < 999999) {
      switch (itemValue) {
        case 'mv':
          this.usMarketValue = '';
          break;
        case 'vv':
          this.usVolumeValue = '';
          break;
      }
      return;
    }
  }

  formatMarketCap(rawValue) {
    const numArr = [rawValue.toString().split('')];
    numArr[0].splice(3, 0, '.');
    const formattedVal = Number(numArr[0].join('')).toFixed(2);

    return +formattedVal;    
  }

  formatVolume(rawValue) {
    const numArr = [rawValue.toString().split('')];
    if(numArr.toString().length >= 5) {
      numArr[0].splice(3, 0, '.');
      numArr.toString().replace('.', ',');  
    }
    if (numArr.toString().length < 5) {
      numArr[0].splice(2, 0, '.');
    }

    const formattedVal = Number(numArr[0].join('')).toFixed(2);

    return +formattedVal;
  }

  chooseArrow(openPrice) {
    if (this.currentPrice > openPrice) {
      this.arrowType = '../assets/images/arrows/group-2-copy-4@3x.png';
      document.getElementById('arrow-direction-change').style.transform = 'rotate(180deg)';
      document.getElementById('arrow-direction-change').style.marginBottom = '3px';
      return;
    }
    if (this.currentPrice < openPrice) {
      this.arrowType = '../assets/images/arrows/path-3-copy@3x.png';
      return;
    }
    this.arrowType = '../assets/images/arrows/Rectangle.png';
  }

  usChooseArrow(usOpenPrice) {
    if (this.usCurrentPrice > usOpenPrice) {
      this.arrowType = '../assets/images/arrows/group-2-copy-4@3x.png';
      document.getElementById('arrow-direction-change').style.transform = 'rotate(180deg)';
      document.getElementById('arrow-direction-change').style.marginBottom = '3px';
      return;
    }
    if (this.usCurrentPrice < usOpenPrice) {
      this.arrowType = '../assets/images/arrows/path-3-copy@3x.png';
      return;
    }
    this.arrowType = '../assets/images/arrows/Rectangle.png';
  }

  getInfo() {
    this.data.getData()
      .subscribe(
        (d: any) => {
          const askPrice = +d.results.quote[0].pricedata.ask;
          const lastPrice = +d.results.quote[0].pricedata.last;

          const usAskPrice = +d.results.quote[1].pricedata.ask;
          const usLastPrice = +d.results.quote[1].pricedata.last;

          this.usShortName = d.results.quote[1].equityinfo.shortname.toString();
          this.shortName = d.results.quote[0].equityinfo.shortname.toString().split(':CNX').join('');

          this.usCurrentPrice = Number((usAskPrice ? usAskPrice : usLastPrice).toFixed(2));
          this.currentPrice = Number((askPrice ? askPrice : lastPrice).toFixed(2));

          this.usChange = +d.results.quote[1].pricedata.change.toFixed(2);
          this.change = +d.results.quote[0].pricedata.change.toFixed(2);

          this.usChangePercent = +d.results.quote[1].pricedata.changepercent.toFixed(2);
          this.changePercent = +d.results.quote[0].pricedata.changepercent.toFixed(2);

          this.usVolume = this.formatVolume(+d.results.quote[1].pricedata.sharevolume);
          this.volume = this.formatVolume(+d.results.quote[0].pricedata.sharevolume);

          this.usMarketCap = Number(this.formatMarketCap(+d.results.quote[1].fundamental.marketcap).toFixed(2));
          this.marketCap = Number(this.formatMarketCap(+d.results.quote[0].fundamental.marketcap).toFixed(2));

          this.usDayRangeHigh = Number((+d.results.quote[1].pricedata.high).toFixed(2));
          this.dayRangeHigh = Number((+d.results.quote[0].pricedata.high).toFixed(2));

          this.usDayRangeLow = Number((+d.results.quote[1].pricedata.low).toFixed(2));
          this.dayRangeLow = Number((+d.results.quote[0].pricedata.low).toFixed(2));

          this.usWeekRangeHigh = Number((+d.results.quote[1].fundamental.week52high.content).toFixed(2));
          this.weekRangeHigh = Number((+d.results.quote[0].fundamental.week52high.content).toFixed(2));

          this.usWeekRangeLow = Number((+d.results.quote[1].fundamental.week52low.content).toFixed(2));
          this.weekRangeLow = Number((+d.results.quote[0].fundamental.week52low.content).toFixed(2));

          const usOpenPrice = +d.results.quote[1].pricedata.open;
          const openPrice = +d.results.quote[0].pricedata.open;

          this.checkValue(+d.results.quote[0].pricedata.sharevolume, this.volumeValue);
          this.usCheckValue(+d.results.quote[1].pricedata.sharevolume, this.usVolumeValue);

          this.checkValue(+d.results.quote[0].fundamental.marketcap, this.marketValue);
          this.usCheckValue(+d.results.quote[1].fundamental.marketcap, this.usMarketValue);

          this.chooseArrow(openPrice);
          this.chooseArrow(usOpenPrice);

          this.date2 = new Date().toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
          console.log(Number((+d.results.quote[1].pricedata.low)));
        }
      );
  }
}

(function () {
  setInterval(function () {
    thisComponent.getInfo();
  }, (10 * 60 * 1000));
})();
