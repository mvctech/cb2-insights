import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sec-fillings',
  templateUrl: './sec-fillings.component.html',
  styleUrls: ['./sec-fillings.component.scss']
})
export class SecFillingsComponent implements OnInit {

  fillings = [
    // { id: 1, title: 'Approval to Commence Public Trading', show: false, date: 'Nov 1, 2018', link: 'PublicTrading.pdf' },
    { id: 1, title: 'Final Prospectus', show: false, date: 'Feb 27, 2019'}
  ];

  constructor(private route: Router) { }

  ngOnInit() {
    window.scroll(0, 0);
  }

  directToDocument(link: any) {
    //console.log(this.route);
    this.route.navigate(['../assets/documents/', link]);
  }

}
