import { Component, OnInit, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {
  @ViewChild('f') contactform: NgForm;

  constructor(private http: Http) { }

  lat: Number = 43.657020;
  lng: Number = -79.599671;
  zoom: Number = 15;
  scrollwheel: Boolean = false;
  gestureHandling: String = 'none';
  document;
  invalid;
  contactForm;

  styles = [
    {
      'featureType': 'all',
      'elementType': 'labels.text.fill',
      'stylers': [
        {
          'saturation': 36
        },
        {
          'color': '#000000'
        },
        {
          'lightness': 40
        }
      ]
    },
    {
      'featureType': 'all',
      'elementType': 'labels.text.stroke',
      'stylers': [
        {
          'visibility': 'on'
        },
        {
          'color': '#000000'
        },
        {
          'lightness': 16
        }
      ]
    },
    {
      'featureType': 'all',
      'elementType': 'labels.icon',
      'stylers': [
        {
          'visibility': 'off'
        }
      ]
    },
    {
      'featureType': 'administrative',
      'elementType': 'geometry.fill',
      'stylers': [
        {
          'color': '#000000'
        },
        {
          'lightness': 20
        }
      ]
    },
    {
      'featureType': 'administrative',
      'elementType': 'geometry.stroke',
      'stylers': [
        {
          'color': '#000000'
        },
        {
          'lightness': 17
        },
        {
          'weight': 1.2
        }
      ]
    },
    {
      'featureType': 'landscape',
      'elementType': 'geometry',
      'stylers': [
        {
          'color': '#000000'
        },
        {
          'lightness': 20
        }
      ]
    },
    {
      'featureType': 'poi',
      'elementType': 'geometry',
      'stylers': [
        {
          'color': '#000000'
        },
        {
          'lightness': 21
        }
      ]
    },
    {
      'featureType': 'road.highway',
      'elementType': 'geometry.fill',
      'stylers': [
        {
          'color': '#000000'
        },
        {
          'lightness': 17
        }
      ]
    },
    {
      'featureType': 'road.highway',
      'elementType': 'geometry.stroke',
      'stylers': [
        {
          'color': '#000000'
        },
        {
          'lightness': 29
        },
        {
          'weight': 0.2
        }
      ]
    },
    {
      'featureType': 'road.arterial',
      'elementType': 'geometry',
      'stylers': [
        {
          'color': '#000000'
        },
        {
          'lightness': 18
        }
      ]
    },
    {
      'featureType': 'road.local',
      'elementType': 'geometry',
      'stylers': [
        {
          'color': '#000000'
        },
        {
          'lightness': 16
        }
      ]
    },
    {
      'featureType': 'transit',
      'elementType': 'geometry',
      'stylers': [
        {
          'color': '#000000'
        },
        {
          'lightness': 19
        }
      ]
    },
    {
      'featureType': 'water',
      'elementType': 'geometry',
      'stylers': [
        {
          'color': '#000000'
        },
        {
          'lightness': 17
        }
      ]
    }
  ];

  ngOnInit() {
    window.scroll(0, 0);
  }

  markerIconUrl() {
    return ('../../assets/images/arrows/group-2-copy.svg');
  }

  private contactURL = 'https://clinic-api-us-prod.sailcannabis.co/api/user/SendEmailToInfoMVC';

  onContactSubmitted(f: NgForm) {
    const data = f.value;

    const info =  {
      Reason: data.reason,
      Message: data.message,
      Subject: 'Contact Form - CB2 Insights',
      Name: data.firstName + ' ' + data.lastName,
      EmailAddress: data.email,
      Phone: data.phone,
      Type: ''
    };

    this.http.post(this.contactURL, info)
      .subscribe(
        (res) => {
          this.contactform.reset();
          console.log('Success: ' + res.status);
          console.log(info);
          document.querySelector('.submit').innerHTML = "Thank You";
        },
        (err) => {
          console.log('Fail: ' + err.status);
          this.invalid = document.querySelectorAll(".ng-invalid");
          this.invalid[0].style.border = '1px solid #c93e3e';
          document.getElementById('input-invalid-message').style.display = 'block';
          window.scroll(0, 0);
        }
      );
  }
}
