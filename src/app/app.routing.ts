import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SplashComponent } from './splash/splash.component';
import { PolicyComponent } from './policy/policy.component';
import { TermsComponent } from './terms/terms.component';
import { PdfPresentationComponent } from './pdf-presentation/pdf-presentation.component';
import { PdfTermsheetComponent } from './pdf-termsheet/pdf-termsheet.component';
import { PdfDataReportComponent } from './pdf-datareports/pdf-datareport.component';
import { PdfDataReportDownloadComponent } from './pdf-datareports/pdf-datareport-download/pdf-datareport-download.component';
import { PdfNaivePatientsComponent } from './pdf-naivepatients/pdf-naivepatients.component';
import { SeniorsSnapShotReportComponent } from './pdf-seniorssnapshot/pdf-seniorssnapshot.component';

import { PdfRWEReportComponent } from './pdf-rwereport/pdf-rwereport.component';
import { PdfSpanishRWEReportComponent } from './pdf-rwereport-spanish/pdf-rwereport-spanish.component';

import { PdfNaivePatientsDownloadComponent } from './pdf-naivepatients/pdf-naivepatients-download/pdf-naivepatients-download.component';
import { DataReportComponent} from './data-reports/datareport.component';

import { StockDataComponent } from './stock-data/stock-data.component';
import { FinancialInfoComponent } from './financial-info/financial-info.component';
import { TeamComponent } from './team/team.component';
import { CareersComponent } from './careers/careers.component';
import { FaqComponent } from './faq/faq.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { InvestorComponent } from './investor/investor.component';

import { StoryComponent } from './story/story.component';
import { NewsComponent } from './news/news.component';
import { NewsArticleComponent } from './news/news-article/news-article.component';
import { JobsComponent } from './jobs/jobs.component';
import { JobPostingsComponent } from './jobs/job-postings/job-postings.component';
import { JobDetailsComponent } from './jobs/job-details/job-details.component';
import { SecFillingsComponent } from './sec-fillings/sec-fillings.component';


import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';

import { AgmCoreModule } from '@agm/core';

const appRoutes: Routes = [
    {
        path: '',
        component: SplashComponent,
        data: {
            title: '',
            description: ''
        }
    },
    {
        path: 'policy', component: PolicyComponent, data: {
            title: '',
            description: ''
        }
    },
    {
        path: 'terms', component: TermsComponent, data: {
            title: '',
            description: ''
        }
    },
    {
        path: 'investorpresentation', component: PdfPresentationComponent, data: {
            title: '',
            description: ''
        }
    },
    {
        path: 'cannabisandmoodreport', component: PdfDataReportComponent, data: {
            title: '',
            description: ''
        }
    },
    {
        path: 'cannabisandmoodreportdownload', component: PdfDataReportDownloadComponent, data: {
            title: '',
            description: ''
        }
    },
    {
        path: 'datareports', component: DataReportComponent, data: {
            title: '',
            description: ''
        }
    },
    {
        path: 'seniorssnapshotreport', component: SeniorsSnapShotReportComponent, data: {
            title: '',
            description: ''
        }
    },
    {
        path: 'rwereport', component: PdfRWEReportComponent, data: {
            title: '',
            description: ''
        }
    },
    {
        path: 'rwe-report', component: PdfSpanishRWEReportComponent, data: {
            title: '',
            description: ''
        }
    },
    {
        path: 'naivepatientreport', component: PdfNaivePatientsComponent, data: {
            title: '',
            description: ''
        }
    },
    {
        path: 'naivepatientreportdownload', component: PdfNaivePatientsDownloadComponent, data: {
            title: '',
            description: ''
        }
    },
    {
        path: 'termsheet', component: PdfTermsheetComponent, data: {
            title: '',
            description: ''
        }
    },
    {
        path: 'stockinfo', component: StockDataComponent, data: {
            title: '',
            description: ''
        }
    },
    {
        path: 'financialinfo', component: FinancialInfoComponent, data: {
            title: '',
            description: ''
        }
    },
    {
        path: 'team', component: TeamComponent, data: {
            title: '',
            description: ''
        }
    },
    {
        path: 'careers', component: CareersComponent, data: {
            title: '',
            description: ''
        }
    },
    {
        path: 'faq', component: FaqComponent, data: {
            title: '',
            description: ''
        }
    },
    {
        path: 'contact', component: ContactUsComponent, data: {
            title: '',
            description: ''
        }
    },
    {
        path: 'investor', component: InvestorComponent, data: {
            title: '',
            description: ''
        }
    },
    {
        path: 'story', component: StoryComponent, data: {
            title: '',
            description: ''
        }
    },
    {
        path: 'news', component: NewsComponent, data: {
            title: '',
            description: ''
        },
    },
    {
        path: 'news/article/:id', component: NewsArticleComponent, data: {
            title: '',
            description: ''
        },
    },
    {
        path: 'jobs', component: JobsComponent, data: {
            title: '',
            description: ''
        },
    },
    {
        path: 'job-postings', component: JobPostingsComponent, data: {
            title: '',
            description: ''
        },
    },
    {
        path: 'jobs/job-details/:id', component: JobDetailsComponent, data: {
            title: '',
            description: ''
        },
    },
    {
        path: 'secfilings', component: SecFillingsComponent, data: {
            title: '',
            description: '',
        }
    },
    {
        path: '**',
        redirectTo: '',
        data: {
          title: '',
        }
    }
];

@NgModule({
    declarations: [
        SplashComponent,
        PolicyComponent,
        TermsComponent,
        SeniorsSnapShotReportComponent,
        DataReportComponent,
        PdfPresentationComponent,
        PdfDataReportComponent,
        PdfDataReportDownloadComponent,
        PdfRWEReportComponent,
        PdfSpanishRWEReportComponent,
        PdfNaivePatientsComponent,
        PdfNaivePatientsDownloadComponent,
        PdfTermsheetComponent,
        StockDataComponent,
        FinancialInfoComponent,
        TeamComponent,
        CareersComponent,
        FaqComponent,
        ContactUsComponent,
        InvestorComponent,
        StoryComponent,
        NewsComponent,
        NewsArticleComponent,
        JobsComponent,
        JobPostingsComponent,
        JobDetailsComponent,
        SecFillingsComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forRoot(appRoutes),
        FormsModule,
        ReactiveFormsModule,
        ScrollToModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyDqHcUp4scDkB_ocegpZWJnPzm7-ZZhBeI'
          }),
    ],
    exports: [RouterModule]
})

export class AppRoutingModule {

}


