import { Component, OnInit, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import { NgForm, FormsModule, Validators, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-investor',
  templateUrl: './investor.component.html',
  styleUrls: ['./investor.component.scss']
})
export class InvestorComponent implements OnInit {
  @ViewChild('f') contactform: NgForm;
  invalid;
  valid;
  textValue;

  constructor(private http: Http) { }

  private contactURL = 'https://clinic-api-ca-prod.sailcannabis.co/api/user/SendEmailToInfoMVC';

  ngOnInit() {
    window.scroll(0, 0);
  }

  onContactSubmitted(f: NgForm) {    
    console.log('Form Submitted');

    const data = this.contactform.value;

    const info =  {
      Message: data.message,
      Subject: 'Investor Presentation Download Form - CB2 Insights',
      Name: data.firstName + ' ' + data.lastName,
      EmailAddress: data.email,
      Phone: data.phone,
      Type: 'InvestorPresentation'
    };

    // this.textValue: string = '';

    // textAreaEmpty(){
    //   if (this.textValue != '') {
    //     console.log(this.textValue);
    //   }
    // }

  this.http.post(this.contactURL, info)
    .subscribe(
      (res) => {
        // if (f.form.valid) {
          console.log('Success: ' + res.status);
          console.log(info);
          this.contactform.reset();
          window.location.href = "https://clinic-api-ca-prod.sailcannabis.co/files/CB2_Investor_Presentation.pdf";
          // this.valid[0].style.border = '1px solid transparent';
          // document.getElementById('input-invalid-message').style.display = 'none';
        // }
      },
      (err) => {
        console.log('Fail: ' + err.status);
        // this.invalid = document.querySelectorAll("input.ng-invalid");
        // this.invalid[0].style.border = '1px solid #c93e3e';
        // document.getElementById('input-invalid-message').style.display = 'block';
        window.scroll(0, 0);
      }
    );
  }
}
