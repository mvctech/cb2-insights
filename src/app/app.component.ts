import { isPlatformBrowser, DOCUMENT } from '@angular/common';
import { environment } from '../environments/environment';
import { WINDOW } from '@ng-toolkit/universal';
import { Component, HostListener, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { StockInformationService } from './shared/services/stock-information.service';
let thisComponent: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  showSubMenuLinks: Boolean = false;
  showInvestorsSubMenuLinks: Boolean = false;

  title = 'cb2-insights';
  showBackToTopButton: boolean;
  showDropdownMenu: Boolean = false;

  subMenu;
  
  usArrowType: String;
  arrowType: String;

  usShortName: String;
  shortName: String;

  usCurrentPrice: Number = 0;
  currentPrice: Number = 0;

  allContent;

  public ngOnInit(): void {
    if (!isPlatformBrowser(this.platformId)) {
      const bases = this.document.getElementsByTagName('base');

      if (bases.length > 0) {
        bases[0].setAttribute('href', environment.baseHref);
      }
    }
    thisComponent = this;
  }

  constructor(@Inject(PLATFORM_ID) private platformId: any, @Inject(DOCUMENT) private document: any
    , @Inject(WINDOW) private window: Window, private data: StockInformationService) {

    this.getInfo();

  }

  @HostListener('document:click', ['$event'])
  clickout(event) {
    /*document.body.addEventListener("touchstart", function (event) {
      document.querySelector('body').style.height = '100%';
      document.querySelector('body').style.overflow = 'hidden';
    }, false);*/

    if ((event.target.id !== 'sideDrawer') && (event.target.id !== 'has_resources_submenu') && (event.target.id !== 'has_investors_submenu')) {
      this.showDropdownMenu = false;
      document.querySelector('body').style.height = 'auto';
      document.querySelector('body').style.overflow = 'scroll';
    }
    if (event.target.id === 'mnu') {
      // document.querySelector('body').style.height = '100%';
      // document.querySelector('body').style.overflow = 'hidden';
    }

    this.allContent = document.getElementsByClassName('.allContent.shrinkScreen');
  }

  @HostListener('window:resize', ['$event'])
  hideSideDrawer() {
    this.showDropdownMenu = false;
  }

  @HostListener('window:scroll', ['$event'])
  checkScroll() {
    const that = this;
    this.window.addEventListener('mousewheel', function (e) {
      const wDelta = e.wheelDelta < 0 ? false : true;
      if (this.window.pageYOffset > 300 && wDelta) {
        return that.showBackToTopButton = wDelta;
      }
      that.showBackToTopButton = false;
    }
    );
  }

  hideButton() {
    this.showBackToTopButton = false;
  }

  menuClick() {
    if (this.showDropdownMenu === false) {
      // document.querySelector('body').style.height = '100%';
      // document.querySelector('body').style.overflow = 'hidden';
      setTimeout(() => {
        this.showDropdownMenu = true;
      }, 1);
    }
  }

  dropdownItemClicked() {
    this.showDropdownMenu = false;
  }

  showSubMenu() {
    console.log('Clicked!');
    if (this.showSubMenuLinks = !this.showSubMenuLinks) {
      document.getElementById('this_resources_submenu').style.display = "block";
    } else {
      document.getElementById('this_resources_submenu').style.display = "none";
    }
  }

  showInvestorsSubMenu() {
    console.log('Clicked!');
    if (this.showInvestorsSubMenuLinks = !this.showInvestorsSubMenuLinks) {
      document.getElementById('this_investors_submenu').style.display = "block";
    } else {
      document.getElementById('this_investors_submenu').style.display = "none";
    }
  }

  chooseArrow(openPrice) {
    if (this.currentPrice > openPrice) {
      this.arrowType = '../assets/images/arrows/group-2-copy-4@3x.png';
      document.getElementById('arrow-direction').style.transform = 'rotate(180deg)';
      document.getElementById('arrow-direction').style.marginBottom = '3px';
      document.getElementById('arrow-direction-side').style.transform = 'rotate(180deg)';
      document.getElementById('arrow-direction-side').style.marginBottom = '3px';
      return;
    }

    if (this.currentPrice < openPrice) {
      this.arrowType = '../assets/images/arrows/path-3-copy@3x.png';
      return;
    }

    else {
      this.arrowType = '../assets/images/arrows/Rectangle.png';
    }
  } 

  usChooseArrow(usOpenPrice) {
    if (this.usCurrentPrice > usOpenPrice) {
      this.usArrowType = '../assets/images/arrows/group-2-copy-4@3x.png';
      document.getElementById('us-arrow-direction').style.transform = 'rotate(180deg)';
      document.getElementById('us-arrow-direction').style.marginBottom = '3px';
      document.getElementById('us-arrow-direction-side').style.transform = 'rotate(180deg)';
      document.getElementById('us-arrow-direction-side').style.marginBottom = '3px';
      return;
    }

    if (this.usCurrentPrice < usOpenPrice) {
      this.usArrowType = '../assets/images/arrows/path-3-copy@3x.png';
      return;
    }

    else {
      this.usArrowType = '../assets/images/arrows/Rectangle.png';
    }
  }

  /**** Stock Info ****/
  getInfo() {
    this.data.getData()
      .subscribe(
        (d: any) => {
          const usAskPrice = +d.results.quote[1].pricedata.ask;
          const askPrice = +d.results.quote[0].pricedata.ask;

          const usLastPrice = +d.results.quote[1].pricedata.last;
          const lastPrice = +d.results.quote[0].pricedata.last;

          this.usShortName = d.results.quote[1].equityinfo.shortname.toString();
          this.shortName = d.results.quote[0].equityinfo.shortname.toString().split(":CNX").join('');

          this.usCurrentPrice = Number((usAskPrice ? usAskPrice : usLastPrice).toFixed(2));
          this.currentPrice = Number((askPrice ? askPrice : lastPrice).toFixed(2));

          const usOpenPrice = +d.results.quote[1].pricedata.open;
          const openPrice = +d.results.quote[0].pricedata.open;

          this.usChooseArrow(usOpenPrice);
          this.chooseArrow(openPrice);
        }
        , err => { console.log(err); });
  }
}

(function () {
  setInterval(function () {
    thisComponent.getInfo();
  }, (10 * 60 * 1000));
})();
