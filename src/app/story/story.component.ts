import {AfterViewInit, Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';


@Component({
  selector: 'app-story',
  templateUrl: './story.component.html',
  styleUrls: ['./story.component.scss']
})
export class StoryComponent implements OnInit, AfterViewInit {

  // tiles = [
  //   {
  //     logo: '../../assets/images/logos/group-6-copy@2x.png',
  //     title: 'Canna Care Docs',
  //     name: 'CCD',
  //     summary: 'North America’s largest and longest-running cannabis evaluation and education centers serving more than 80,000 patients a year in 12 states.',
  //     url: 'https://cannacaredocs.com/',
  //     arrowSrc: '../../assets/images/arrows/group-copy@2x.png'
  //   },
  //   {
  //     logo: '../../assets/images/logos/group-17-copy@2x.png',
  //     title: 'Sail',
  //     name: 'Sail',
  //     summary: 'Practice Management software for use with specialty clinics and general practitioners to evaluate patients for medical cannabis with ease.',
  //     url: 'http://sailcannabis.co',
  //     arrowSrc: '../../assets/images/arrows/group@2x.png'
  //   },
  //   {
  //     logo: '../../assets/images/logos/group-9-copy@2x.png',
  //     title: 'TokeIn',
  //     name: 'TokeIn',
  //     summary: 'The industry’s leading app-based loyalty and CRM platform used by cannabis retailers throughout the US.',
  //     url: 'http://tokein.com/',
  //     arrowSrc: '../../assets/images/arrows/group-copy-2@2x.png'
  //   }
  // ];

  tiles = [
    {
      logo: '../../assets/images/logos/group-6-copy@2x.png',
      title: 'Clinical Operations',
      name: 'Clinical Operations',
      summary: 'We own and operate North America’s largest and longest-running cannabis evaluation and education centers serving more than 65,000 patients a year in 12 states.',
      url: 'https://cannacaredocs.com/',
      arrowSrc: '../../assets/images/arrows/group-copy@2x.png'
    },
    {
      logo: '../../assets/images/logos/sail_tokein_logo@3x.png',
      //logo2: '../../assets/images/logos/group-9-copy@3x.png',
      title: 'Proprietary Technology',
      name: 'Proprietary Technology',
      summary: 'By developing the industry’s only cannabis-focused practice management software, data collection tools and consumer loyalty platform we ensure that we have full control of the data we collect from evaluation to follow up.',
      url: 'https://cannacaredocs.com/',
      arrowSrc: '../../assets/images/arrows/group-copy@2x.png'
    },
    {
      logo: '../../assets/images/logos/CB2-logo3x.png',
      title: 'Data Insights',
      name: 'Data Insights',
      summary: 'We are data-driven in everything we do.  Not only do we use our insights to improve our technologies and patient care, but we build analytics tools that can be used throughout the industry to advance the understanding of how cannabis functions as a medicine.',
      url: 'https://cannacaredocs.com/',
      arrowSrc: '../../assets/images/arrows/group-copy@2x.png'
    }
  ];

  @ViewChild('dataPoints') dataPoints: ElementRef;
  public patientsNumberOne = 0;
  public patientsNumberTwo = 0;
  public patientsNumberThree = 0;
  public patientsNumberFour = 0;

  public patientsCounterOne = 0;
  public patientsCounterTwo = 0;
  public patientsCounterThree = 0;
  public patientsCounterFour = 0;

  public isPatientVisible: BehaviorSubject<boolean>;

  constructor() {
    this.isPatientVisible = new BehaviorSubject<boolean>(false);
  }

  @HostListener('window:scroll', [])
  onWindowScroll() {
    const scrollPositionPlus = window.pageYOffset + 700;
    const componentPosition = this.dataPoints.nativeElement.offsetTop;
    if (!this.isPatientVisible.value) {
      if (scrollPositionPlus >= componentPosition) {
        this.isPatientVisible.next(true);
      }
    }
  }

  public patientObs(): Observable<boolean> {
    return this.isPatientVisible.asObservable();
  }

  ngOnInit() {
    window.scroll(0, 0);
    this.patientObs().subscribe((value: Boolean) => {
      if (value) {
        this.patientIncrementNumberOne();
        this.patientIncrementNumberTwo();
        this.patientIncrementNumberThree();
        this.patientIncrementNumberFour();
      }
    });
  }

  ngAfterViewInit() {
  }

  patientIncrementNumberOne() {
    setTimeout(() => {
      this.patientsNumberOne = this.patientsCounterOne;
      this.patientsCounterOne++;
      if (this.patientsCounterOne <= 18) {
        this.patientIncrementNumberOne();
      }
    }, 80);
  }

  patientIncrementNumberTwo() {
    setTimeout(() => {
      this.patientsNumberTwo = this.patientsCounterTwo;
      this.patientsCounterTwo++;
      if (this.patientsCounterTwo < 336) {
        this.patientIncrementNumberTwo();
      }
    }, 1);
  }

   patientIncrementNumberThree() {
    setTimeout(() => {
      this.patientsNumberThree = this.patientsCounterThree;
      this.patientsCounterThree++;
      if (this.patientsCounterThree < 28) {
        this.patientIncrementNumberThree();
      }
    }, 60);
  }

   patientIncrementNumberFour() {
    setTimeout(() => {
      this.patientsNumberFour = this.patientsCounterFour;
      this.patientsCounterFour++;
      if (this.patientsCounterFour < 421) {
        this.patientIncrementNumberFour();
      }
    }, 1);
  }
}



