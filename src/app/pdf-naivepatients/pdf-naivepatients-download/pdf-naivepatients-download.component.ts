import { Component, OnInit } from '@angular/core';
import { GoogleAnalyticsService } from '../../shared/services/google-analytics.service';

@Component({
  selector: 'app-pdf-naivepatients-download',
  templateUrl: './pdf-naivepatients-download.component.html',
  styleUrls: ['./pdf-naivepatients-download.component.scss']
}) 
export class PdfNaivePatientsDownloadComponent implements OnInit {
  // url: string;

  //constructor(private analytics: GoogleAnalyticsService) { }

  ngOnInit() {
    window.scroll(0, 0);
  }
}
