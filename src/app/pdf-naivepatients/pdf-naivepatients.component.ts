import { Component, OnInit, ViewChild } from '@angular/core';
import { GoogleAnalyticsService } from '../shared/services/google-analytics.service';
import { Http } from '@angular/http';
import { NgForm, FormBuilder, FormsModule, Validators, FormGroup, FormControl } from '@angular/forms';
import { invalid } from '@angular/compiler/src/render3/view/util';

@Component({
  selector: 'app-pdf-naivepatients',
  templateUrl: './pdf-naivepatients.component.html',
  styleUrls: ['./pdf-naivepatients.component.scss']
})
export class PdfNaivePatientsComponent implements OnInit {
  @ViewChild('f') contactform: NgForm;
  url: string;

  constructor(private analytics: GoogleAnalyticsService, private http: Http, private fb: FormBuilder) { }

  document;
  invalid;
  valid;
  form;

  private contactURL = 'https://clinic-api-ca-prod.sailcannabis.co/api/user/SendEmailToInfoMVC';

  ngOnInit() {
    window.scroll(0, 0);
  }

  onContactSubmitted(f: NgForm) {
    console.log('Button Clicked');
    const data = f.value;

    const info = {
      Message: data.message,
      Subject: 'CB2 Naive Patient - Data Report Download Form',
      Name: data.firstName + ' ' + data.lastName,
      EmailAddress: data.email,
      Phone: data.phone,
      Type: 'DataReport'
    };

    this.http.post(this.contactURL, info)
    .subscribe(
      (res) => {
        // if (f.form.valid) {
          console.log('Success: ' + res.status);
          console.log(info);
          this.contactform.reset();
          window.location.href = "https://clinic-api-ca-prod.sailcannabis.co/files/CB2_Data_Report.pdf";
        // }
      },
      (err) => {
        console.log('Fail: ' + err.status);
        // this.invalid = document.querySelectorAll("input.ng-invalid");
        // this.invalid[0].style.border = '1px solid #c93e3e';
        // document.getElementById('input-invalid-message').style.display = 'block';
        window.scroll(0, 0);
      }
    );
  }
}
