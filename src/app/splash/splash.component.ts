import { Component, OnInit } from '@angular/core';
import { GoogleAnalyticsService } from '../shared/services/google-analytics.service';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.component.html',
  styleUrls: ['./splash.component.scss']
})
export class SplashComponent implements OnInit {

  tiles = [
    {
      logo: '../../assets/images/logos/ccd_logos@3x.jpg',
      title: 'Clinical Solutions',
      name: 'Clinical Solutions',
      summary: 'We offer end-to-end solutions for clinical trials from trial design and management to monitoring, development and research. We own and operate the largest network of cannabis certification clinics in North America and have built the largest medical cannabis patient registry in the US with 100,000 unique patients per year. Using our extensive experience in building patient registries, we can build your patient recruitment strategy and clinic network.',
      url: 'https://cannacaredocs.com/',
      arrowSrc: '../../assets/images/arrows/group-copy@2x.png'
    },
    {
      logo: '../../assets/images/logos/sail_tokein_logo@3x.png',
      //logo2: '../../assets/images/logos/group-9-copy@3x.png',
      title: 'Data Solutions',
      name: 'Data Solutions',
      summary: "Manage every aspect of the clinical trial process with our suite of proprietary technology solutions. The research workflow is fully integrated with our technology and can be used for standardized data collection management, analyses and insights. The existing infrastructure within our technology and processes can produce both retrospective and prospective research, and generate Real World Evidence and publications.",
      url: 'https://cannacaredocs.com/',
      arrowSrc: '../../assets/images/arrows/group-copy@2x.png'
    }
    // {
    //   logo: '../../assets/images/logos/CB2-logo3x.png',
    //   title: 'Data Insights',
    //   name: 'Data Insights',
    //   summary: 'We are data-driven in everything we do.  Not only do we use our insights to improve our technologies and patient care, but we build analytics tools that can be used throughout the industry to advance the understanding of how cannabis functions as a medicine.',
    //   url: 'https://cannacaredocs.com/',
    //   arrowSrc: '../../assets/images/arrows/group-copy@2x.png'
    // }
    // {
    //   logo: '../../assets/images/logos/group-9-copy@2x.png',
    //   title: 'TokeIn',
    //   name: 'TokeIn',
    //   summary: 'The industry’s leading app-based loyalty and CRM platform used by cannabis retailers throughout the US.',
    //   url: 'http://tokein.com/',
    //   arrowSrc: '../../assets/images/arrows/group-copy-2@2x.png'
    // }
  ];

  additional = [
  // {
  //   image: '3-stock.jpg',
  //   title: 'Stock Info',
  //   color: '#98c93e',
  //   summary: 'Get real-time stock data for CB2 Insights.',
  //   url: '/stockinfo'
  // },
  // {
  //   image: '4-financial.jpg',
  //   title: 'Financial Information',
  //   color: '#30bbad',
  //   summary: 'View our Financial Reports and Earnings Releases.',
  //   url: '/financialinfo'
  // },
  {
    image: 'story-tile@3x.png',
    title: 'The CB2 Story',
    color: '#98c93e',
    //summary: 'Driving the Cannabis Industry through Business Intelligence.',
    summary: 'Working to mainstream medical cannabis into traditional healthcare.',
    url: '/story'
  },
  {
    image: 'team-tile@3x.png',
    title: 'Leadership',
    color: '#30bbad',
    summary: 'Our team is the heart and soul of what we do.',
    url: '/team'
  },
  {
    image: 'news-tile@3x.png',
    title: 'Newsroom',
    color: '#296a6d',
    summary: 'Our latest news releases and media coverage.',
    url: '/news'
  },
  {
    image: 'careers-tile@3x.png',
    title: 'Careers',
    color: '#afafaf',
    summary: 'We’re always looking to expand our team.',
    url: '/careers'
  }
  ];

  // additional = [{
  //   image: '../../assets/images/jumbotronbg/terms2x.jpg',
  //   title: 'StockData',
  //   color: '#98c93e',
  //   summary: 'this is a summary',
  //   link: ''
  // },
  // {
  //   image: '../../assets/images/jumbotronbg/terms2x.jpg',
  //   title: 'Financial Information',
  //   color: '#30bbad',
  //   summary: 'this is a summary',
  //   link: ''
  // },
  // {
  //   image: '../../assets/images/jumbotronbg/terms2x.jpg',
  //   title: 'Newsroom',
  //   color: '#296a6d',
  //   summary: 'this is a summary',
  //   link: ''
  // },
  // {
  //   image: '../../assets/images/jumbotronbg/terms2x.jpg',
  //   title: 'Careers',
  //   color: '#afafaf',
  //   summary: 'this is a summary',
  //   link: ''
  // }];

  constructor(private analytics: GoogleAnalyticsService) { }

  ngOnInit() {
    window.scroll(0, 0);
  }
}
