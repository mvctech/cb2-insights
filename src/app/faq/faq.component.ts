import { Component, OnInit } from '@angular/core';
import { FaqService } from '../shared/services/faq.service';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {

  stock = [];
  financial = [];

  constructor(private faq: FaqService) {
    this.stock = faq.data1;
    this.financial = faq.data2;
  }

  ngOnInit() {
    window.scroll(0, 0);
  }

  accordianClicked(event) {
    const id = +event.target.id;

    for (const d of this.stock) {
      if (id === d.id) {
        d.show = d.show = !d.show;
      } else {
        d.show = false;
      }
    }

    for (const e of this.financial) {
      if (id === e.id) {
        e.show = e.show = !e.show;
      } else {
        e.show = false;
      }
    }
  }

}
