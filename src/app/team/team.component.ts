import { Component, OnInit, HostListener } from '@angular/core';
import { jitExpression } from '@angular/compiler';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit {

  images = [
    {
      id: 0, name: 'Prad Sekar', url: 'waliedsoliman@2x.png', title: 'Chief Executive Officer',
      contentBackground: '#ffffff', fontChange: '#575757',
      profile: 'As co-founder of CB2 Insights, Prad has spent more than 15 years as an entrepreneur throughout' 
      + ' the healthcare sector from owning and operating medical clinics through to consulting on major IT integrations.'  
      + ' Prad has become one of the leading voices in how to advance healthcare practices through objective data and predictive analytics.',
      contentDisplayed: false,
    },
    {
      id: 1, name: 'Kash Qureshi', url: 'david-danziger-copy@2x.png', title: 'Chief Operating Officer',
      contentBackground: '#ffffff', fontChange: '#575757',
      profile: 'As co-founder of CB2 Insights, Kash brings more than 20 years of extensive operational experience in the healthcare,' 
      + ' medical cannabis and technology space.  An ardent cost-efficiency executive, Kash merges international sales and business development skills' 
      + ' with improving cashflow and operational proficiencies to drive overall profitability.',
      contentDisplayed: false,
    },
    {
      id: 2, name: 'Dan Thompson', url: 'kash-copy@2x.png', title: 'Chief Corporate Officer',
      contentBackground: '#ffffff', fontChange: '#575757',
      profile: 'Dan brings over 15 years of marketing experience with high-growth public and private technology companies. Dan built'
      + ' and led the marketing divisions for early-stage companies in retail, travel and payment technology. Dan graduated from'
      + ' Michigan State University with degrees in marketing and communications.',
      contentDisplayed: false,
    },
    {
      id: 3, name: 'Carmelo Marrelli', url: 'pradyum-copy@2x.png', title: 'Chief Financial Officer',
      contentBackground: '#ffffff', fontChange: '#575757',
      profile: 'Carmello (Carm) is the principal of Marrelli Support Services Inc. In addition, Carm is affiliated with DSA Corporate'
        + ' Services Inc., a firm providing corporate secretarial and regulatory filing services to the junior capital market in Canada.'
        + ' He has a Bachelor of Commerce degree from the University of Toronto.',
      contentDisplayed: false,
    }
  ];

  board = [
    { name: 'Kash Qureshi', title: 'COO, CB2 Insights', position: 'Chairman of the Board', other: 'Audit Committee' },
    { name: 'David Danziger', title: 'SVP Assurance, MNP LLP', position: '', other: 'Audit Committee Chair' },
    { name: 'Prad Sekar', title: 'CEO, CB2 Insights', position: 'Compensation Committee', other: 'Nomination Committee' },
    {
      name: 'Dr. Danial Schecter', title: 'Executive Director, CMClinic Chief Medical Advisor, AusCann', position: '',
      other: 'Compensation Committee Chair', other2: 'Nomination Committee Chair'
    }
  ];

  colors = [
    { val: '#30BBAD' },
    { val: '#98C93E' },
    { val: '#296A6D' }
  ];

  selectedColor;

  imgList = [
    { imgUrl: 'img-leadership-1@3x.png' },
    { imgUrl: 'img-leadership-2@3x.png' },
    { imgUrl: 'img-leadership-3@3x.png' },
    { imgUrl: 'img-leadership-4@3x.png' },

    // { imgUrl: 'sail-office-16@2x.jpg' },
    // { imgUrl: 'sail-office-32@2x.jpg' },
    // { imgUrl: 'sail-office-29-copy@2x.jpg' },
    // { imgUrl: 'sail-office-15@2x.jpg' },
  ];

  constructor() { }

  ngOnInit() {
    window.scroll(0, 0);
    this.selectedColor = '#ffffff';
  }

  getRandomColor(event) {
    for (const j of this.images) {
      if (+event.target.id === +j.id) {
        j.contentBackground = this.colors[Math.floor(Math.random() * this.colors.length)].val;
        j.fontChange = '#ffffff';
      }
      else {
        j.contentDisplayed = false;
        j.contentBackground = '#ffffff';
        j.fontChange = '#575757';
      }
    }
  }

  resetColor() {
    for (const j of this.images) {
      j.contentBackground = '#ffffff';
      j.fontChange = '#575757';
    }
  }

  showProfile(event) {
    for (const k of this.images) {
      if (+event.target.id === +k.id) {
        k.contentDisplayed = !k.contentDisplayed;
      }
      else {
        k.contentDisplayed = false;
        k.contentBackground = '#ffffff';
        k.fontChange = '#575757';
      }
    }
  }
}





















