import { Component, OnInit } from '@angular/core';
import { GoogleAnalyticsService } from '../../shared/services/google-analytics.service';

@Component({
  selector: 'app-pdf-datareport-download',
  templateUrl: './pdf-datareport-download.component.html',
  styleUrls: ['./pdf-datareport-download.component.scss']
}) 
export class PdfDataReportDownloadComponent implements OnInit {
  // url: string;

  //constructor(private analytics: GoogleAnalyticsService) { }

  ngOnInit() {
    window.scroll(0, 0);
  }
}
