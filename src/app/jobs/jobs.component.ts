import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.scss']
})
export class JobsComponent implements OnInit {

  teamSelected: String = '';
  citySelected: String = '';

  constructor() { }

  ngOnInit() {
    window.scroll(0, 0);
    this.citySelected = 'Sauga';
    this.teamSelected = 'Who knows?';
  }

  optionChangeTeam(event) {
    console.log('changed Team');
    alert("Changed Team");
  }

  optionChangeCity(event) {
    console.log('changed City');
    alert("Changed City");
  }

}
