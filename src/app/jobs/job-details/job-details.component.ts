import { Component, OnInit, ViewChild, Input } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {WordpressService} from '../../shared/services/wordpress.service';
import { Http } from '@angular/http';
import { NgForm, FormGroup } from '@angular/forms';
//import { Config } from '../../config/constant';
// import {JobPostingsComponent} from '../job-postings/job-postings.component';

@Component({
  selector: 'app-job-details',
  templateUrl: './job-details.component.html',
  styleUrls: ['./job-details.component.scss']
})
export class JobDetailsComponent implements OnInit {
  @ViewChild('f') contactform: NgForm;
  private url = '';

  showSpinner: boolean;
  btnText: string;
  errorMsg = '';

  jobPostings;
  title;
  team;
  content;
  file;
  resumeFile;
  tempID;
  count;
  show;
  tempFilterValue;
  form: FormGroup;
  invalid;

  selectedFile: File = null;
  fileChosen = false;
  document: any;

  id: any;
  myParams;

  constructor(private wp: WordpressService, private route: Router, private activatedRoute: ActivatedRoute, private http: Http) { }

  ngOnInit() {
  window.scroll(0, 0);
  console.log(this.route);
  // console.log(this.activatedRoute.params.subscribe(params: Params ) => (){ this.myParam = params['id']});
  this.activatedRoute.params
    .subscribe((params: Params) => {
        this.id = +params['id'];
        this.url = 'https://mvc-ca-web-stag.azurewebsites.net/wp-json/wp/v2/posts/' + this.id;
        //this.getArticleDetails();
        console.log(this.id);
      });

  this.wp.getData()
  .subscribe(
    (d: any) => {
      this.jobPostings = d;
      console.log(d);
      console.log(d[0].categories[0]);
      for (let i = 0; i < d.length; i++) {
        console.log(d[i].categories[i]);
        this.show = true;
      }
    });
    this.createNewDocument();
  }

  onFileSelected(event: any) {
    this.file = document.querySelector('input#resume');
    document.getElementById('resumeFile').innerHTML = this.file.files[0].name;

    this.fileChosen = true;
    this.document.fileName = event.target.files[0].name;
    this.document.fileType = this.document.fileName.substr(this.document.fileName.lastIndexOf('.') + 1);

    let filename = event.target.files[0].name;
    filename = filename.substring(0, filename.lastIndexOf('.'));
    this.document.fileName = filename;

    this.selectedFile = <File>event.target.files[0];
    const file = event.dataTransfer ? event.dataTransfer.files[0] : event.target.files[0];
    const reader = new FileReader();
    if (!file) {
      return;
    }
    reader.onload = this.handleReaderLoaded.bind(this);
    reader.readAsArrayBuffer(this.selectedFile);
  }

  handleReaderLoaded(e: any) {
    const reader = e.target;
    this.document.filebinary = reader.result;
  }

  createNewDocument() {
    this.document = {
      file: '',
      type: '',
      title: '',
      fileType: '',
      notes: '',
      filebinary: '',
      fileName: '',
      created_date: ''
    };
    this.errorMsg = '';
    this.fileChosen = false;
  }

/*  getArticleDetails() {
    return this.http.get(this.url)
      .pipe(map(
        (res: Response) => {
          const data = res.json();
          data.date = this.wp.getDate(data.date);
          this.title = data.title.rendered;
          this.content = data.content.rendered;
        }
      )).subscribe();
  }*/

  private contactURL = 'https://clinic-api-us-prod.sailcannabis.co/api/user/SendAttachmentEmailToInfoMVC';

  /*uploadDocument(event: any) {
    this.file = document.querySelector('input#resume');
    
    console.log(this.file.value);

    document.getElementById('resumeFile').innerHTML = this.file.files[0].name;
    console.log(this.file);
    // if (event.target.files && event.target.files[0]) {
    //   const reader = new FileReader();
    //   reader.onload = () => {
    //     this.form.get('file').setValue(event.target.files[0]);
    //   };
    //   reader.readAsDataURL(event.target.files[0]);
    //   console.log(reader.result);
    // }
  }*/


onContactSubmitted(f: NgForm) {

  const data = f.value;

  const queryStr = '?&msg=' + data.message + '&from=' + data.firstName + data.lastName + '&email=' + data.email + '&fn=' + this.document.fileName + '&ft=' + this.document.fileType + '&position=' + this.title;

    // const info =  {
    //   Message: data.message,
    //   Subject: 'Contact Form - CB2 Insights',
    //   Name: data.firstName + ' ' + data.lastName,
    //   EmailAddress: data.email,
    //   Resume: data.resume,
    //   Type: ''
    // };

  this.http.post(this.contactURL + queryStr, this.document.filebinary)
    .subscribe(
      (res) => {
        this.contactform.reset();
        console.log('Success: ' + res.status);
        console.log(data);
        console.log(queryStr);
        // document.getElementById('submit').style.display = 'none';
        // document.getElementById('download-report').style.display = 'block';
      },
      (err) => {
        console.log('Fail: ' + err.status);
        this.invalid = document.querySelectorAll("input.ng-invalid");
        this.invalid[0].style.border = '1px solid #c93e3e';
        document.getElementById('input-invalid-message').style.display = 'block';
      }
    );
  }
}






















