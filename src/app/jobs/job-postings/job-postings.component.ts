import { Component, OnInit, HostListener } from '@angular/core';
import {WordpressService} from '../../shared/services/wordpress.service';
import {Router} from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-job-postings',
  templateUrl: './job-postings.component.html',
  styleUrls: ['./job-postings.component.scss']
})

export class JobPostingsComponent implements OnInit {
  jobPostings;
  title;
  team;
  content;
  tempID;
  count;
  show;
  tempFilterValue;

  constructor(private wp: WordpressService, private route: Router) {
    this.getJobPostings();
  }

  ngOnInit() {
    window.scroll(0, 0);
  }

  getJobPostings() {
    this.wp.getData()
    .subscribe(
      (d: any) => {
        this.jobPostings = d;
        console.log(d);
        console.log(d[0].categories[0]);
        for (let i = 0; i < d.length; i++) {
          //console.log(d[i].categories[i]);
          if (d[i].categories[0] !== 19) {
            this.jobPostings[i].categories = d[i].categories[0];
            //console.log(d[0].categories[0]);
            this.show = true;
          } else {
            this.jobPostings[i].categories = d[i].categories[1];
          }
        }
      }
    );

    this.wp.getNames()
    .subscribe(
      (d: any) => {
        this.team = d;
      }
    );

    this.wp.getDesc()
    .subscribe(
      (b: any) => {
        this.content = b;
        //console.log(this.content);
      }
    );
  }

  showSelectOptions(event) {
    document.querySelector('ul').style.display = "block";
  }

  selectTeam(event) {
    const dataValue = event.target.getAttribute('data-value');
    const filterButton = document.querySelector('button');
    console.log(dataValue);
    document.querySelector('.team_select_field p').innerHTML = dataValue;
    document.querySelector('ul.team')[0].style.display = "none";
  }

  selectCity(event) {
    const dataValue = event.target.getAttribute('data-value');
    const filterButton = document.querySelector('button');
    console.log(dataValue);
    document.querySelector('.city_select_field p').innerHTML = dataValue;
    document.querySelector('ul.city')[0].style.display = "none";
  }

  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (event.target.id !== 'team_select_field') {
      document.querySelector('ul').style.display = "none";
    }
    if (event.target.id !== 'city_select_field') {
      document.querySelector('ul.city')[0].style.display = "none";
    }
  }

  optionChangeTeam(event) {
    const value = event.target.value;
    const teams = document.getElementById(value).id;
    // this.show = false;
    for (const t of this.team) {
      this.tempID = value;
      if (value === teams) {
        //this.show = false;
      }
    }
    event.preventDefault();
  }

  filterOption(event) {
    const teamValue = document.getElementById("team_select_field").innerHTML;
    const teams = document.getElementsByClassName("teams");

    // for (const t of this.team) {
      this.tempFilterValue = teamValue;
      // alert(this.tempFilterValue);
      
      for (let i = 0; i < teams.length; i++) {
        
        if ((this.tempFilterValue === teams[i].id) || (this.tempFilterValue === "All")) {
          //console.log(teams[i].id);
          document.getElementById(teams[i].id).style.display = "block";
        }
        else if (this.tempFilterValue !== teams[i].id) {
          document.getElementById(teams[i].id).style.display = "none";
          //alert("There is a match!");
        }
      // }
    }
    event.preventDefault();
  }

  accordianClicked(event) {
    const id = +event.target.id;
    for (const d of this.team) {
      if (id === d.id) {
        if (d.count > 0) {
          d.show = d.show = !d.show;
        }
      } else {
        d.show = false;
      }
    }
  }

  directToDetails(id: any) {
    // console.log(this.route);
    this.route.navigate(['jobs/job-details', id]);
  }
}











