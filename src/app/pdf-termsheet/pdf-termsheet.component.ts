import { Component, OnInit } from '@angular/core';
import { GoogleAnalyticsService } from '../shared/services/google-analytics.service';

@Component({
  selector: 'app-pdf-termsheet',
  templateUrl: './pdf-termsheet.component.html',
  styleUrls: ['./pdf-termsheet.component.scss']
})
export class PdfTermsheetComponent implements OnInit {

  constructor(private analytics: GoogleAnalyticsService) { }

  ngOnInit() {
    window.scroll(0, 0);
  }

}
