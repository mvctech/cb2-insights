import { Component, OnInit } from '@angular/core';
import { GoogleAnalyticsService } from '../shared/services/google-analytics.service';

@Component({
  selector: 'app-pdf-presentation',
  templateUrl: './pdf-presentation.component.html',
  styleUrls: ['./pdf-presentation.component.scss']
})
export class PdfPresentationComponent implements OnInit {

  url: string;

  constructor(private analytics: GoogleAnalyticsService) { }

  ngOnInit() {
    window.scroll(0, 0);
  }

}
