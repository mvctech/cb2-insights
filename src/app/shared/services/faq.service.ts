import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FaqService {

  constructor() { }

  data1 = [
    {
      id: 1,
      show: false,
      q: 'What exchange are CB2 Insights’ common shares listed on, and what is the ticker symbol?',
      a: 'Canadian Securities Exchange:  CBII <br>  OTCQB: CBIIF'
    },
    {
      id: 2,
      show: false,
      q: 'Does CB2 Insights pay a cash dividend?',
      a: 'CB2 Insights does not currently pay a dividend.'
    },
    {
      id: 3,
      show: false,
      q: 'How do I buy and sell CB2 Insights shares?',
      a: 'CB2 Insights common shares can be bought and sold through any stock broker where our shares are listed.'
    },
    {
      id: 4,
      show: false,
      q: 'Can I purchase stock directly from CB2 Insights?',
      a: 'No, but CB2 Insights stock can be purchased through just about any brokerage firm, including online brokerage services.'
    },
    {
      id: 5,
      show: false,
      q: 'How can I get the current CB2 Insights stock price?',
      a: 'A 20 minute delayed price as well as other information regarding CB2 Insights stock is provided by Quote Media and can be seen <a href="stockinfo" class="green-span" style="color: ">here</a>.'
    },
    {
      id: 6,
      show: false,
      q: 'When did CB2 Insights become a public company?',
      a: 'CB2 Insights became publicly traded on March 5, 2019.'
    },
    {
      id: 7,
      show: false,
      q: 'Is there currently any preferred stock outstanding?',
      a: 'No.'
    },
    {
      id: 8,
      show: false,
      q: 'How do I contact CB2 Insights’ transfer agent?',
      a: 'CB2 Insights has appointed Capital Transfer Agency, ULC (“CTA”) to provide registrar, transfer agent and shareholder services directly to our registered shareholders. If you are a registered shareholder, they can help you with a variety of shareholder-related services including change of address, transfer of stock ownership, lost or missing stock certificate(s) and account statement requests.'        
        + '<br><br><div class="">Capital Transfer Agency ULC</div>'
        + '<div class="">390 Bay Street, Suite 920</div>'
        + '<div class="">Toronto, ON  M5H 2Y2</div>'
        + '<div class=""><a href="mailto:info@capitaltransferagency.com">info@capitaltransferagency.com</a></div>'
        + '<div class="">(416) 350-5007</div>'
        + '<div class=""><a href="http://capitaltransferagency.com/" target="_blank">http://capitaltransferagency.com/</a></div>'
    },
    {
      id: 9,
      show: false,
      q: 'How do I obtain information on my registered shareholdings?',
      a: 'CB2 Insights has appointed Sophic Capital as its Investor Relations firm which will provide you with information related to your current holdings or other information that shareholders may require. Alternately, you may contact our Transfer Agent as listed above.<br><br>'
      + 'Sophic Capital<br>'
      + '<a href="mailto:investors@cb2insights.com">investors@cb2insights.com</a><br>'
      + '(647) 957-2327'
    },
  ];

  data2 = [
    {
      id: 10,
      show: false,
      q: 'When does your fiscal year end?',
      a: 'CB2 Insights’ fiscal year runs from January 1 to December 31.'
    },
    {
      id: 11,
      show: false,
      q: 'When are your quarterly reporting dates for this year?',
      a: 'Q1 – May<br>'
        + 'Q2 – August<br>'
        + 'Q3 – November<br>'
        + 'Q4 – April<br>'
        + 'Specific dates to be determined. CB2 Insights will make a public announcement regarding any earnings release dates approximately 5 business days prior to the release.'
      // a: 'Q1 – May 4, 2018<br>'
      //   + 'Q2 – August 2, 2018<br>'
      //   + 'Q3 – November 2, 2018<br>'
      //   + 'Q4 – February 2019 <br>(specific date to be determined)'
      //   + 'These dates are subject to change.  Future dates will be posted the day after YE reports are released.'
    },
    // {
    //   id: 12,
    //   show: false,
    //   q: ' Where can I view CB2 Insights’ latest quarterly financial results?',
    //   a: '<a href="/financialinfo">Click here</a> to view the latest quarterly financial results.'
    // },
    {
      id: 13,
      show: false,
      q: 'How do I access CB2 Insights’ regulatory filings?',
      a: 'You can access all regulatory filings on <a href="http://sedar.com">Sedar.com</a> posted under CB2 Insights.'
    },
    {
      id: 24,
      show: false,
      q: 'Who is the Company’s independent auditor?',
      a: 'Grant Thornton was appointed as the auditor for the financial year 2018. The auditor is nominated by management and voted on at the Annual Meeting of Shareholders.'
    },
    // {
    //   id: 14,
    //   show: false,
    //   q: 'Where can I obtain the Annual Report to Shareholders and/or the Annual Information Form and Proxy Circular?',
    //   a: 'CB2 Insights has yet to release its Annual Report.  It intends to do so on or around April 2019.'
    //    + 'Please <a href="/financialinfo">click here</a> to access the Proxy Circular in the in the Financial Reports section.'
    //   + 'Please <a href="/financialinfo">click here</a> to access the Annual Information Form in the Financial Reports section.'
    //     + 'To request a hard copy mailed to you, please email our Corporate Communications department at <a href="mailto:dan.thompson@cb2insights.com">dan.thompson@cb2insights.com.</a>'
    // },
    {
      id: 14,
      show: false,
      q: 'Where can I obtain the Annual Report to Shareholders and/or the Annual Information Form and Proxy Circular?',
      a: 'CB2 Insights has yet to release its Annual Report. It intends to do so in the month of April.'
        + ' Once the Annual General Meeting of Shareholders has been scheduled, you will be able to access the Annual Information Form and Proxy Circular on the Financial Info page.To request a hard copy be mailed to you, please email our Investor Relations department at <a href="mailto:investors@cb2insights.com">investors@cb2insights.com</a>.'
    },
    // {
    //   id: 15,
    //   show: false,
    //   q: 'Where can I view CB2 Insights’ latest quarterly financial results?',
    //   a: '<a href="/financialinfo">Click here</a> to view the latest quarterly financial results.'
    // },
    // {
    //   id: 16,
    //   show: false,
    //   q: 'How do I access CB2 Insights’ regulatory filings?',
    //   a: 'Please <a href="/financialinfo">click here</a> to access the Company’s Financial Reports.  You can also access SEDAR as sedar.com. At the bottom of this page, click “View This Public Company’s Documents” for a complete list of CB2 Insights’ regulatory filings.'
    // },
    // {
    //   id: 17,
    //   show: false,
    //   q: 'Who is the Company’s independent auditor?',
    //   a: 'Grant Thornton was appointed as the auditor for the financial year 2018. The auditor is nominated by management and voted on at the Annual Meeting of Shareholders.'
    // },
    // {
    //   id: 18,
    //   show: false,
    //   q: 'Where can I obtain the Annual Report to Shareholders and/or the Annual Information Form and Proxy Circular?',
    //   a:
    //      'CB2 Insights has yet to release its Annual Report.  It intends to do so on or around April 2019.'
    //      + 'Please <a href="/financialinfo">click here</a> to access the Proxy Circular in the in the Financial Reports section.'
    //      + 'Please <a href="/financialinfo">click here</a> to access the Annual Information Form in the Financial Reports section.'
    //      + 'To request a hard copy mailed to you, please email our Corporate Communications department at <a href="mailto:dan.thompson@cb2insights.com">dan.thompson@cb2insights.com.</a>',
    // },
    {
      id: 19,
      show: false,
      q: 'When is the next annual meeting of shareholders?',
      a: 'CB2 Insights expects to have its next annual meeting of shareholders in June of 2019.  We will announce the date and location of the meeting when we file our proxy statement.'
    },
    {
      id: 20,
      show: false,
      q: 'Where can I find the investor presentation?',
      a: 'Please <a href="/investor">click here</a> to access the Presentations page.',
    },
    {
      id: 21,
      show: false,
      q: 'Where can I view senior executives’ bios?',
      a: 'Please <a href="/team">click here</a> for the executive bios.',
    },
    {
      id: 22,
      show: false,
      q: 'Where can I find employment information?',
      a: 'Please <a href="/careers">click here</a> to view current openings and access the Careers section.',
    },
    {
      id: 23,
      show: false,
      q: 'Who are the equity analysts that cover CB2 Insights?',
      a: 'Please check back frequently to find new analyst coverage of CB2 Insights.',
    }
  ];
}
