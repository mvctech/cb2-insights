import { Injectable, AfterViewInit } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})

export class StockInformationService {
  private QM = 'https://app.quotemedia.com/data/getQuotes.json?symbols=CBII:CNX,CBIIF&webmasterId=103219';  
  private observable: Observable<any>;

  constructor(private http: Http) { }

  ngAfterViewInit(): void {
  }

  getData() {
    // return this.http.get(this.QM)
    //   .pipe(map(
    //     (response: Response) => {
    //       return response.json();
    //     }
    //   ));

    if (this.observable) {
      // if `this.observable` is set then the request is in progress
      // return the `Observable` for the ongoing request
      return this.observable;
    } else {
      // example header (not necessary)
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      // create the request, store the `Observable` for subsequent subscribers
      this.observable = this.http.get(this.QM, {
        headers: headers
      })
        .pipe(map(response => {
          // when the cached data is available we don't need the `Observable` reference anymore
          this.observable = null;

          if (response.status == 400) {
            return "FAILURE";
          } else if (response.status == 200) {
            //this.data = new Data(response.json());
            //return this.data;
            return response.json();
          }
          // make it shared so more than one subscriber can get the result
        }))
        .share();
      return this.observable;
    }
  }  
}
