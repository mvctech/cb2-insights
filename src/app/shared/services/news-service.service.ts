import { Injectable, OnInit } from '@angular/core';
import { WordpressService } from '../services/wordpress.service';
import { Content } from '@angular/compiler/src/render3/r3_ast';

@Injectable({
  providedIn: 'root'
})
export class NewsServiceService implements OnInit {
  
  ns = [
    // {
    //   imgUrl: 'news_post_3.jpg',
    //   header: 'Data Report',
    //   title: 'New Data from CB2 Insights Shows Majority of Patients Seeking Cannabis for Mood-Related Conditions',
    //   date: 'January 15, 2019',
    //   url: '/news/article/3',
    // },
    {
      imgUrl: 'jonathan-velasquez-160775-unsplash@2x.png',
      header: 'In the News',
      title: 'KNX InDepth Radio Interview',
      date: 'October 17, 2018',
      url: 'https://omny.fm/shows/knx-in-depth/knx-indepth-october-17-2018?in_playlist=podcast',
    },
    {
      imgUrl: 'owen-farmer-543732-unsplash@2x.png',
      header: 'In the News',
      title: 'Canada\'s New Legal Weed Economy is a Testing Ground for the U.S. Market',
      date: 'October 17, 2018',
      url: 'https://www.forbes.com/sites/irisdorbian/2018/10/17/canadas-new-legal-weed-economy-should-boost-u-s-market-maybe/#287a2ea96a38',
    },
    {
      imgUrl: 'group-2@2x.png',
      header: 'Announcement',
      title: 'KCSA Strategic Communications\' Phil Carlson to Host the "Public Companies Symposium" at the New West Summit with Alan Brochstein of 420 Investor',
      date: 'October 9, 2018',
      url: 'https://www.prnewswire.com/news-releases/kcsa-strategic-communications-phil-carlson-to-host-the-public-companies-symposium-at-the-new-west-summit-with-alan-brochstein-of-420-investor-300727550.html',
    },
    // {
    //   imgUrl: '4-mvc-technologies-announces-rebrand-1@2x.png',
    //   header: 'Announcement',
    //   title: 'MVC Technologies Announces Rebrand to CB2 Insights with New Focus on Predictive Analytics for the Cannabis Industry',
    //   date: 'October 3, 2018',
    //   url: '/news/article/1',
    // },
    // {
    //   imgUrl: '5-tokein@2x.png',
    //   header: 'Announcement',
    //   title: 'MVC Technologies Acquires Consumer Engagement and Loyalty Platform Developed for Global Cannabis Retailers',
    //   date: 'October 3, 2018',
    //   url: '/news/article/2',
    // },
    {
      imgUrl: '3-kcsa-strategic-communications-2@2x.png',
      header: 'In the News',
      title: 'MVC Technologies, with $5 Million Sales in First Half of 2018, Intends to Go Public',
      date: 'September 10, 2018',
      url: 'https://www.newcannabisventures.com/mvc-technologies-with-5-million-sales-in-first-half-of-2018-intends-to-go-public/',
    },
    {
      imgUrl: '7-legalization-further-affirms-the-improtance-2@2x.png',
      header: 'Article',
      title: 'Legalization further affirms the importance of Canada’s medical cannabis framework',
      date: 'August 10, 2018',
      url: 'https://www.thegrowthop.com/cannabis-health/cannabis-medical/legalization-further-affirms-the-importance-of-canadas-medical-cannabis-framework',
    },
    {
      imgUrl: 'rawpixel-656709-unsplash@2x.png',
      header: 'In the News',
      title: 'CGOC announces investment in cannabis healthcare technology leader Sail',
      date: 'July 11, 2018',
      url: 'https://www.newswire.ca/news-releases/cgoc-announces-investment-in-cannabis-healthcare-technology-leader-sail-687881281.html',
    },
    {
      imgUrl: 'shane-rounce-390475-unsplash@2x.png',
      header: 'In the News',
      title: 'Sail Cannabis CEO Prad Sekar Is Bringing Medical Cannabis Into Mainstream Health Care For Better Patient Care',
      date: 'July 13, 2018',
      url: 'https://www.benzinga.com/fintech/18/07/12017781/sail-cannabis-ceo-prad-sekar-is-bringing-medical-cannabis-into-mainstream-hea',
    },
    {
      imgUrl: 'sail@2x.png',
      header: 'In the News',
      title: 'Sail Clinical Decision Support Platform Selected by the Association of Cannabis Specialists',
      date: 'May 16, 2018',
      url: 'https://www.newcannabisventures.com/sail-clinical-decision-support-platform-selected-by-the-association-of-cannabis-specialists/',
    }
  ];

  fullArtcles = [
    {
      id: 1,
      type: 'Announcement',
      date: 'October 9, 2018',
      title: 'MVC Technologies Announces Rebrand to CB2 Insights with New Focus on Predictive Analytics for the Cannabis Industry',
      subtitle: 'Cannabis Technology Company Introduces New Corporate Identity and Outlines Market Strategy for its House of Brands',
      image: '../../assets/images/news/4-mvc-technologies-announces-rebrand-1@3x.jpg',
      content: '<p>TORONTO, Oct. 03, 2018 (GLOBE NEWSWIRE) -- MVC Technologies Inc. (“MVC”) today announced that it will undergo'
        + ' a significantbrand transformation with the introduction of a corporate rebrand and a re-emphasis on the strategies of'
        + ' its current group of branded cannabis industry software solutions and services. The new entity, CB2 Insights, will focus'
        + ' on business intelligence and predictive analytics tools across the cannabis value chain while its sub-brands, Sail'
        + ' Cannabis, Canna Care Docs and TokeIn will continue to support the cannabis ecosystem within their respective verticals.</p>'

        + '<p>“We have seen substantial growth, both within our own organization and no doubt, in the global cannabis industry at-large,”'
        + ' said Prad Sekar, Chief Executive Officer, CB2 Insights. “While each of our established brands continues to grow within Canada,'
        + ' the US and internationally, we now have a substantial opportunity to leverage these assets to build predictive analytics tools'
        + ' to fill the deep void of trusted data currently available in this highly fragmented industry. CB2 Insights’ mission is to'
        + ' ensure that big data is central to the evolution of the industry, as it is more needed today than it has ever been before.” <p>'

        + 'The Company will move forward with a strategy where its parent brand will amass data from each of its sub-brands while those'
        + ' sub-brands will work independently within their respective vertical. This allows the Company to continue to serve licensed'
        + ' producers, retailers, clinicians and patients whether by software or services, while CB2 Insights focuses on building tools'
        + ' from the data that each brand collects, driving actionable insights across the ecosystem.'

        + '<div class=aritlceHeading> Sail Cannabis</div>'

        + '<p>Already deployed domestically and internationally, the Sail Cannabis EMR platform is the leading suite of cannabis-focused'
        + ' clinical practice management tools in the industry.  Sail allows clinics to manage their entire clinical operation through a'
        + ' single platform designed specifically for the cannabis market. The platform also aims to assist clinicians in integrating'
        + ' cannabis into their practice through its data-driven Clinical Decision Support platform which includes evaluation tools,'
        + ' dosing guidelines and other unique features. Sail is active today in multiple global markets and supports more than 80,000'
        + ' patient  encounters each year. </p>'

        + '<div class=aritlceHeading>Canna Care Docs</div>'

        + '<p>Canna Care Docs (“CCD”) is a serviced-based offering that operates a network of US-based, multi-state brick and mortar'
        + ' cannabis evaluation and education centres as well as telemedicine applications in some states.  CCD serves a core need within'
        + ' the industry as a specialized network of trained clinicians supporting communities through the qualification,'
        + ' recommendation/prescription of medical cannabis and education to patients looking to integrate cannabis into their treatment'
        + ' regimen. CCD also offers a fully managed service model of pre-and-post-education services through virtual consultation to'
        + ' assist independent clinicians in their own practice, saving them time on education so that they can focus solely on patient'
        + ' care.</p>'
    },
    {
      id: 2,
      type: 'Announcement',
      date: 'September 26, 2018',
      title: 'MVC Technologies Acquires Consumer Engagement and Loyalty Platform Developed for Global Cannabis Retailers',
      image: '../../assets/images/news/5-tokein@3x.jpg',
      content: '<p>TORONTO, Sept. 26, 2018 (GLOBE NEWSWIRE) -- MVC Technologies Inc. (“MVC”), a leading cannabis-focused technology'
        + ' and services company, today announced that it has acquired customer loyalty, retention and engagement platform, TokeIn.'
        + ' TokeIn enables dispensaries and other cannabis retailers to engage with their customers with the industry’s only mobile app-based'
        + ' platform.  The acquisition was completed earlier this month.</p>'

        + '<p>Currently available on iOS and Android, dispensaries are able to target customers through building their own branded'
        + ' rewards program, opt-in customized deals and other engagement tools pushed through a variety of communication channels.'
        + ' The platform moves MVC further upstream in the cannabis value chain, adding to the company’s cannabis EMR platform (Sail) and'
        + ' multi-state cannabis evaluation and education centers (Canna Care Docs).</p>'

        + '<p>“TokeIn entered the market less than a year ago and has already proven that it is the exact consumer engagement tool'
        + ' that the industry requires with retailers in multiple states choosing the platform,” said Prad Sekar, Chief Executive Officer,'
        + ' MVC Technologies.  “As various retail models begin to evolve within Canada, the US and abroad, dispensaries, licensed producers'
        + ' and other cannabis retail channels are seeking ways to acquire and retain customers in ways that traditional retailers currently'
        + ' find enormous success with.  We are thrilled to add TokeIn to our portfolio of data-centric products and services to meet this'
        + ' immediate industry need.”</p>'

        + '<p>TokeIn was designed, developed and deployed by a team led by cannabis entrepreneur Peter Barbosa.  Barbosa had previously'
        + ' built a full-scale cannabis seed-to-sale software platform, Grow One, that was subsequently acquired by cannabis technology firm'
        + ' in 2017.  Throughout the due diligence process, it became evident to MVC that the ability to acquire the talent associated wit'
        + ' TokeIn was as significant as the platform itself.  As such, Peter Barbosa will join MVC as its Chief Technology Officer, overseeing'
        + ' the development of TokeIn and all other software products the company offers.</p>'

        + '<p>“We built TokeIn with a very defined goal, to give cannabis retailers the tools they needed to interact with their customers'
        + ' in a more meaningful and lucrative way,” said Barbosa, Chief Technology Officer, MVC Technologies.  “Now being apart of the mission'
        + ' of MVC only solidifies that calling to advance the way that cannabis industry stakeholders use data that can not only meet their'
        + ' immediate needs but propel them to higher levels as the market grows.”</p>'

        + '<p>MVC Technologies is the parent company of a series of brands which serve each vertical within the cannabis value chain.'
        + ' It’s data-driven software and services help licenced producers, clinicians, patients, consumers and now retailers better manage'
        + ' their business or simplify their needs in the market.  The company and its brands have supported more than 280,000 patient encounters'
        + ' across 18 jurisdictions including in Canada, the US and international markets.</p>'

        + '<div class=aritlceHeading>About MVC Technologies</div>'

        + '<p>MVC Technologies has a mission to simplify the medical cannabis journey across the value chain through a suite of technology'
        + ' products and services delivered through three distinct brands – each serving a unique vertical. Sail offers a comprehensive'
        + ' cannabis EMR platform and patient engagement tools to support clinics and clinicians across multiple countries. Canna Care Docs'
        + ' is a leading cannabis group of evaluation and education centres operating in 12 jurisdictions. Canna Care Docs serves more than'
        + ' 65,000 patients annually looking to integrate cannabis into their treatment regimen. TokeIn is the industry’s only mobile app-based'
        + ' customer loyalty and engagement platform used by cannabis retailers.  The parent brand, MVC, works to build predictive analytics '
        + ' tools based on the controlled data input ingested from each of its sub-brands which are used to deliver actionable insights both '
        + ' to its customer base and the industry at-large. For more information please visit <a href="www.sailcannabis.co" target="_blank">www.sailcannabis.co</a>,'
        + ' <a href="www.cannacaredocs.com" target="_blank">www.cannacaredocs.com</a> and <a href="www.tokein.com" target="_blank">www.tokein.com</a>.</p>'

        + '<div class=aritlceHeading>Forward Looking Statement</div>'

        + '<p>This news release may contain forward-looking statements that are based on the Company\'s expectations, estimates'
        + ' and projections regarding its business and the economic environment in which it operates. These statements are not a'
        + ' guarantee of future performance and involve risks and uncertainties that are difficult to control or predict. Therefore,'
        + ' actual outcomes and results may differ materially from those expressed in these forward-looking statements and readers'
        + ' should not place undue reliance on such statements. Statements speak only as of the date on which they are made, and the'
        + ' Company undertakes no obligation to update them publicly to reflect new information or the occurrence of future events or'
        + ' circumstances, unless otherwise required to do so by law.</p>'

        + '<p>For more information, please contact:</p>'

        + '<div>Dan Thompson</div>'
        + '<div>Chief Marketing Officer</div>'
        + '<div>1-416-670-9316</div>'
    },
    {
      id: 3,
      type: 'Data Report',
      date: 'January 15, 2019',
      title: 'New Data from CB2 Insights Shows Majority of Patients Seeking Cannabis for Mood-Related Conditions',
      image: '../../assets/images/news/news_post_3.jpg',
      content: '<p><strong>Leading Global Cannabis Data Company Publishes Real World Evidence Report on Trends in Medical Cannabis and Depression, Anxiety and PTSD</strong></p>'
        + '<p> TORONTO, Jan. 15, 2019(GLOBE NEWSWIRE) -- <a href="https://www.globenewswire.com/Tracker?data=kKJGIfVCIYhKLoPwDkLEzVI1rg1F345_FaDOfX-PIkZaPspdJMBwJC06SLUNPccOvlHu6fy2KDnkTdLZhhfs6g==" target="_blank" rel="nofollow noopener">CB2 Insights</a> (“CB2” or the “Company”) a leading global provider of predictive analytics tools, data - driven software and comprehensive services across the medical cannabis value chain, has released, “<a title="" href="https://www.globenewswire.com/Tracker?data=BSWjyAbHmt_ksN2Lr3_n_UFR9mivKsT62lCdGXUUkMSwIRXFTJ8ELE8XkQoZcCBpwh2LHMoKyhAbtN-6r3wtMFUDMdTj_nwIyrJ3_kH98lvNNNO1Hmm7vUTSVBtW_GYH84NlZbFNQVIPTATIn5qTcX0RPB4Yu-NgOtmqawQQUko_pa3GKFc5Z36buLvQuUtr" target="_blank" rel="nofollow noopener">Real - World Evidence Shows Patients Seeking Cannabis For Mood Disorders</a>,” its first report in a comprehensive series outlining trends and insights found within the medical cannabis industry.The report, which was conducted over a 4 - week period, focuses on the propensity of mood - related conditions in medical cannabis patients and provides comparative analysis with other qualifying primary conditions and condition categories such as sleep, pain, appetite and others.</p>'

        + '<p>According to the study, CB2 Insights found mood - related conditions accounted for the majority(34.77 percent) of patients seeking cannabis as a medicine(conditions such as depression, anxiety, and post - traumatic stress disorder), followed by pain - related.However, while more than half of US states(33 plus Washington, DC) have medical cannabis frameworks in effect, only 24 states list any mood - related disorder as a qualifying condition and in all of those cases only PTSD is listed.</p>'

        + '<p>“As the US continues to progress with new states creating frameworks for medical cannabis usage, the industry is primed for Real - World Evidence(RWE) on the benefits on medical cannabis, which is credible data that the marketplace lacks.As of today, the discrepancy between legislative constraints and patient needs is huge, ” said Prad Sekar, CEO of CB2 Insights.“RWE vital to ensure that patients, healthcare professionals and other stakeholders are able to access appropriate intelligence to better integrate this new therapy into traditional treatment plans.”</p>'

        + '<p>Key highlights of the report include:</p>'

        + '<ul type="disc"><li>Followed by mood - related conditions, pain - related conditions were a close second(33.05 %) which includes conditions such as migraines, chronic pain, back problems and spinal cord injuries.</li>'
        + '<li> According to the study, 15.33 % sought cannabinoid therapy for sleep - related disorders.While most states have approved chronic and intractable pain as a qualifying condition for cannabis usage, the vast majority list PTSD as the only qualifying mood - related condition and two states do not allow for mood - related conditions to qualify at all.</li>'
        + '<li> 7 states plus Washington, DC allow for a certified healthcare practitioner to provide a medical document for patients they deem cannabis as a treatment to be appropriate for any condition; thus allowing those with broader mood - related conditions to qualify.</li></ul>'

        + '<p>CB2 Insights focuses on bridging the evidence - based data gap within the medical cannabis industry through predictive analytics tools obtained from the Company’s software and service offerings.The Company’s sub - brands have reached more than 300, 000 cannabis patient interactions worldwide.</p>'

        + '<p>To download a copy of the report <span class="italic-text">“Real - World Evidence Shows Patients Seeking Cannabis for Mood Disorders”</span> please visit: <a title="" href="http://www.cb2insights.com/cannabisandmoodreport" target="_blank">www.cb2insights.com/cannabisandmoodreport</a>.</p>'

        + '<strong>Methodology:</strong ></p>'

        + '<p>Patients were studied over a 4 - week period across 4 states and out of 16 evaluation and education centers run by CB2 Insights’ subsidiary, Canna Care Docs.</p>'

        + '<p>Within the report, CB2 Insights assessed nearly 500 patients across multiple states within the CB2’s medical cannabis evaluation and education centers, Canna Care Docs.Patients who visit Canna Care Docs seeking comprehensive evaluation and education services are seen by both certified clinicians and educators to assess primary and secondary conditions, symptoms, previous treatments, current medication and many other data points that are used for confidential treatment planning for the patient.</p>'

        + '<p><strong>About CB2 Insights</strong></p>'

        + '<p>CB2 Insights has a mission to simplify the medical cannabis journey across the value chain through a suite of healthcare technology products and services delivered through three distinct brands – each serving a unique vertical.Sail offers a comprehensive cannabis - specific practice management platform and data collection tools across multiple countries.Canna Care Docs is a leading cannabis evaluation and education group operating in 12 jurisdictions.Canna Care Docs has served more than 265, 000 patients looking to integrate cannabis into their treatment regimen.TokeIn is the industry’s only app - based customer loyalty and engagement platform used by cannabis retailers.The parent brand, CB2 Insights, works to build predictive analytics tools based on the controlled data input ingested from each of its sub - brands which are used to deliver actionable insights both to its customer base and the industry at - large.For more information please visit <a  href="http://www.cb2insights.com"> www.cb2insights.com </a>, <a href="http://www.sailcannabis.co">www.sailcannabis.co</a>, <a title="www.cannacaredocs.com" href="https://www.globenewswire.com/Tracker?data=GWU472G0r3-t-nFglmyaTFc0IU4qc-zgkqWxI44qC9rVq0qiLKM5IloiVmizg2FZQyuWnGYUGM5xG1iJyw7Kq3kGtNpGveNbGEnV9Ec_vC8=" target="_blank" rel="nofollow noopener">www.cannacaredocs.com</a> and <a title="www.tokein.com" href="https://www.globenewswire.com/Tracker?data=miNC2r221hBbPnMtli2CSzEvsFpVxcaV_5bykJDOCySwHwsuyfcdtTDetPb-8cKQBsepfF2EC0SOk8LwemPupw==" target="_blank">www.tokein.com</a>.</p>'

        + '<br><br><p><strong>For Media Inquiries:</strong></p>'
        + '<div>Kate Tumino / Raquel Cona</div>'
        + '<div>KCSA Strategic Communications</div>'
        + '<div><a href="mailto:ktumino@kcsa.com">ktumino@kcsa.com</a> / <a href="mailto:rcona@kcsa.com">rcona@kcsa.com</a></div>'

        + '<br><br><p><strong>For Other Inquiries:</strong></p>'
        + '<div>Dan Thompson</div>'
        + '<div>Chief Marketing Officer, CB2 Insights</div>'
        + '<div><a href="mailto:dan.thompson@cb2insights.com">dan.thompson@cb2insights.com</a></div>'
    }
  ];

  ngOnInit() {
    window.scroll(0, 0);
  }

  constructor() {
  }
}
