import { AfterViewInit, Component, Injectable,  Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Router, NavigationEnd } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
// export class GoogleAnalyticsService implements AfterViewInit {
export class GoogleAnalyticsService {

  // constructor(@Inject(PLATFORM_ID) private platformId: Object, private router: Router) {}
  
  constructor(private router: Router) { 
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        (<any>window).ga('set', 'page', event.urlAfterRedirects);
        (<any>window).ga('send', 'pageview');
      }
    });
  }

  // ngAfterViewInit(): void {
  //   this.router.events.subscribe(event => {
  //     if (event instanceof NavigationEnd) {
  //       (<any>window).ga('set', 'page', event.urlAfterRedirects);
  //       (<any>window).ga('send', 'pageview');
  //     }
  //   });
  // }
}
