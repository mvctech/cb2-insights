import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class WordpressService {
  private urlNames = 'https://mvc-ca-web-stag.azurewebsites.net/wp-json/wp/v2/categories?per_page=100&parent=19';
  private url = 'https://mvc-ca-web-stag.azurewebsites.net/wp-json/wp/v2/posts?per_page=100&categories=19&fields=id,content,title,categories,parent,meta_box';
  private newsPosts = 'https://mvc-ca-web-stag.azurewebsites.net/wp-json/wp/v2/posts?per_page=100&categories=11&fields=id,content,title,categories,parent,date,attachments,images,media,better_featured_image';
  private externalPosts = 'https://mvc-ca-web-stag.azurewebsites.net/wp-json/wp/v2/posts?per_page=100&categories=33';

  constructor(private http: Http) { }

  getData() {
    return this.http.get(this.url)
      .pipe(map(
        (response: Response) => {
          return response.json();
        }
      ));
    }

  getNames() {
    return this.http.get(this.urlNames)
      .pipe(map(
        (response: Response) => {
          return response.json();
        }
      ));
    }

  getDesc() {
    return this.http.get(this.url)
      .pipe(map(
        (response: Response) => {
          return response.json();
        }
      ));
    }

  getCount() {
    return this.http.get(this.url)
      .pipe(map(
        (response: Response) => {
          return response.json();
        }
      ));
    }

  getNewsPosts() {
    return this.http.get(this.newsPosts)
      .pipe(map(
        (response: Response) => {
          return response.json();
        }
      ));
  }

  getExternalPosts() {
    return this.http.get(this.externalPosts)
      .pipe(map(
        (response: Response) => {
          return response.json();
        }
      ));
  }
}
