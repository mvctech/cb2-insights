import { NgtUniversalModule } from '@ng-toolkit/universal';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from './app.routing';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';

// import { TokenInterceptor } from '@angular/token.interceptor';

import { CookieService } from 'ngx-cookie-service';
import { ServerService } from './shared/services/server.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ErrorHandler, Injectable } from '@angular/core';


@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
  ],
  imports: [
    CommonModule,
    NgtUniversalModule,
    BrowserModule.withServerTransition({appId: 'cb2-insights'}),
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    ScrollToModule.forRoot(),
  ],
  // providers: [ServerService]
  providers: [ CookieService ]
})

// @Injectable()
// export class ServerService {
//   constructor(private http: Http) {}
//   storeServers(servers: any[]) {
//     return this.http.post('https://udemy-ng-http.firebaseio.com/', servers);
//   }
// }

export class AppModule { }



