import { WINDOW } from '@ng-toolkit/universal';
import { Component, OnInit , Inject} from '@angular/core';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';
import { GoogleAnalyticsService } from '../shared/services/google-analytics.service';

@Component({
  selector: 'app-policy',
  templateUrl: './policy.component.html',
  styleUrls: ['./policy.component.scss']
})
export class PolicyComponent implements OnInit {

  FilterOptionSelected: string;

  filterSelected1: boolean;
  filterSelected2: boolean;
  filterSelected3: boolean;
  filterSelected4: boolean;
  filterSelected5: boolean;
  filterSelected6: boolean;
  filterSelected7: boolean;
  filterSelected8: boolean;
  filterSelected9: boolean;
  filterSelected10: boolean;
  filterSelected11: boolean;
  filterSelected12: boolean;
  filterSelected13: boolean;

  constructor(
    @Inject(WINDOW) private window: Window, private _scrollToService: ScrollToService, private analytics: GoogleAnalyticsService) { }

  ngOnInit() {
    window.scroll(0, 0);
    this.FilterOptionSelected = 'Personal Information';
    this.filterSelected1 = true;
  }

  filterClicked(event) {
    // reset all
    this.filterSelected1 = false;
    this.filterSelected2 = false;
    this.filterSelected3 = false;
    this.filterSelected4 = false;
    this.filterSelected5 = false;
    this.filterSelected6 = false;
    this.filterSelected7 = false;
    this.filterSelected8 = false;
    this.filterSelected9 = false;
    this.filterSelected10 = false;
    this.filterSelected11 = false;
    this.filterSelected12 = false;
    this.filterSelected13 = false;

    const target = event.target || event.srcElement || event.currentTarget;

    switch (target.id) {
      case '1':
        this.filterSelected1 = true;
        break;
      case '2':
        this.filterSelected2 = true;
        break;
      case '3':
        this.filterSelected3 = true;
        break;
      case '4':
        this.filterSelected4 = true;
        break;
      case '5':
        this.filterSelected5 = true;
        break;
      case '6':
        this.filterSelected6 = true;
        break;
      case '7':
        this.filterSelected7 = true;
        break;
      case '8':
        this.filterSelected8 = true;
        break;
      case '9':
        this.filterSelected9 = true;
        break;
      case '10':
        this.filterSelected10 = true;
        break;
      case '11':
        this.filterSelected11 = true;
        break;
      case '12':
        this.filterSelected12 = true;
        break;
      case '13':
        this.filterSelected13 = true;
        break;
    }
  }

  optionChange(event) {
    this.FilterOptionSelected = event.target.selectedOptions[0].innerHTML;

    const config: ScrollToConfigOptions = {
      target: event.target.value
    };

    this._scrollToService.scrollTo(config);
  }
}
